

#include <stdint.h>
#include "PLL.h"
#include "UART.h"
#include "bt_serial.h"
#include "tm4c123_z.h"
   
void OutCRLF(void){
  UART_OutChar(CR);
  UART_OutChar(LF);
}

int main(void){
  PLL_Init();              
  UART_Init();              
  BT_Init();
  char c;
  while(1){
    c=BT_InChar();
    UART_OutChar(c);
    if(c=='1')
      set_GPIO_DOUT(GPIOF_APB,0xff);
    else if(c=='0')
      set_GPIO_DOUT(GPIOF_APB,0x00);
      

  }
}

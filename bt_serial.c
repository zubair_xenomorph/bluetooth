
#include <stdint.h>
#include "bt_serial.h"
#include "UART.h"
#include "tm4c123gh6pm.h"


#define UART_FR_TXFF            0x00000020  
#define UART_FR_RXFE            0x00000010  
#define UART_LCRH_WLEN_8        0x00000060  
#define UART_LCRH_FEN           0x00000010  
#define UART_CTL_UARTEN         0x00000001  


void BT_Init(void){
  SYSCTL_RCGCUART_R |= 0x02;            
  SYSCTL_RCGCGPIO_R |= 0x02;            
  while((SYSCTL_PRGPIO_R&0x02) == 0){};
  UART1_CTL_R &= ~UART_CTL_UARTEN;     
  UART1_IBRD_R = 81;                    
  UART1_FBRD_R = 25;                     
  UART1_LCRH_R = (UART_LCRH_WLEN_8|UART_LCRH_FEN);
  UART1_CTL_R |= UART_CTL_UARTEN;       
  GPIO_PORTB_AFSEL_R |= 0x03;           
  GPIO_PORTB_DEN_R |= 0x03;             
  GPIO_PORTB_PCTL_R = (GPIO_PORTB_PCTL_R&0xFFFFFF00)+0x00000011;
  GPIO_PORTB_AMSEL_R &= ~0x03;          
}

char BT_InChar(void){
  while((UART1_FR_R&UART_FR_RXFE) != 0);
  return((char)(UART1_DR_R&0xFF));
}

void BT_OutChar(char data){
  while((UART1_FR_R&UART_FR_TXFF) != 0);
  UART1_DR_R = data;
}

void BT_InString(char *bufPt, uint16_t max) {
int length=0;
char character;
  character = BT_InChar();
  while(character != CR){
    if(character == BS){
      if(length){
        bufPt--;
        length--;
        BT_OutChar(BS);
      }
    }
    else if(length < max){
      *bufPt = character;
      bufPt++;
      length++;
      BT_OutChar(character);
    }
    character = BT_InChar();
  }
  *bufPt = 0;
}

void BT_OutString(char *pt){
  while(*pt){
    BT_OutChar(*pt);
    pt++;
  }
}

